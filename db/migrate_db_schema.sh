# Backup creds
BACKUP_DB_HOST=
BACKUP_DB_PORT=
BACKUP_DB_USER=
BACKUP_DB_PASS=
BACKUP_DB_NAME=

# Restore creds
RESTORE_DB_HOST=
RESTORE_DB_PORT=
RESTORE_DB_USER=
RESTORE_DB_PASS=
RESTORE_DB_NAME=

# Keep same schema name
SCHEMA_NAME=

echo "Re-creating schema $SCHEMA_NAME on database $RESTORE_DB_NAME host $RESTORE_DB_HOST"

PGPASSWORD="$RESTORE_DB_PASS" psql -v ON_ERROR_STOP=1 \
--host "$RESTORE_DB_HOST" --username "$RESTORE_DB_USER" --dbname "$RESTORE_DB_NAME" <<-EOSQL
  CREATE SCHEMA IF NOT EXISTS "$SCHEMA_NAME";
  ALTER SCHEMA "$SCHEMA_NAME" OWNER TO "$RESTORE_DB_USER";
EOSQL

echo "Backup and restoring schema $SCHEMA_NAME from $BACKUP_DB_HOST via pipe to $RESTORE_DB_HOST."

PGPASSWORD="$BACKUP_DB_PASS" pg_dump --verbose --format c \
  --host "$BACKUP_DB_HOST" --port "$BACKUP_DB_PORT" \
  --username "$BACKUP_DB_USER" --schema "$SCHEMA_NAME" "$BACKUP_DB_NAME" \
| PGPASSWORD="$RESTORE_DB_PASS" pg_restore --verbose \
  --no-owner --exit-on-error \
  --host "$RESTORE_DB_HOST" --port "$RESTORE_DB_PORT" \
  --username "$RESTORE_DB_USER" --schema "$SCHEMA_NAME" --dbname "$RESTORE_DB_NAME"
