#/bin/bash

# Official instructions do not work due to naming conflict (duck package exists)
curl -LO https://dist.duck.sh/duck_8.1.1.36550-1_amd64.deb
sudo apt install -f ./duck_8.1.1.36550-1_amd64.deb
sudo ln -s /usr/local/bin/duck /usr/bin/duck

# Use CLI: duck --username <KEY> --list s3://os.zhdk.cloud.switch.ch/envidat02/
