# Gitlab Runner

To run:
- Modify .env variables
    - HOST_NAME: the name of the current machine (used in runner names/tags)
    - REMOTE_DOCKER_HOST: the hostname of a remote machine to run Docker commands
    - INTERNAL_REG: registry to build/push image to
    - EXTERNAL_REG: registry to source images from, default docker.io
- Uncomment / add additional runners in docker-compose.yml if required
- If the REMOTE_DOCKER_HOST variable is set, also add the id_rsa secret to allow the SSH connection
    - id_rsa is the private SSH key of the remote Docker host