#!/bin/bash
set -euo pipefail

export DOCKER_BUILDKIT=1
export COMPOSE_DOCKER_CLI_BUILD=1
DOCKER_GROUP=$(getent group docker | cut -d: -f3)
source .env

docker-compose build --build-arg DOCKER_GROUP=${DOCKER_GROUP}
docker push "${INTERNAL_REG}/gitlab-runner:${GITLAB_RUNNER_TAG}"