#!/bin/bash
set -euo pipefail

CONFIG_FILE="/etc/gitlab-runner/config.toml"


if [[ $# -eq 0 ]]; then
  echo "No arguments supplied."
  if [[ ! -e ${CONFIG_FILE} ]]; then

    # Add env vars for Gitlab registration
    if [[ -f "/run/secrets/gitlab_reg_creds" ]]; then
      echo "Sourcing and exporting /run/secrets/gitlab_reg_creds environment variables"
      set -a
      . /run/secrets/gitlab_reg_creds
    else
      echo "gitlab_reg_creds secret not present. Registration will fail..."
    fi

    echo "Registering runner with $CI_SERVER_URL"
    gitlab-runner register \
      --non-interactive \
      --docker-volumes /var/run/docker.sock:/var/run/docker.sock:ro \
      --docker-volumes /root/.docker/config.json:/root/.docker/config.json:ro \
      --docker-volumes /net:/net
    chmod 777 ${CONFIG_FILE}

  else
    echo "Config file found at: $CONFIG_FILE. Skipping registration..."
  fi

  # Add SSH key (ensure ssh-copy-id to machines prior)
  if [[ -f "/run/secrets/id_rsa" ]]; then
    echo "Copying /run/secrets/id_rsa to ~/.ssh/id_rsa"
    cat /run/secrets/id_rsa > /home/gitlab-runner/.ssh/id_rsa
    chown gitlab-runner:gitlab-runner /home/gitlab-runner/.ssh/id_rsa
    chmod 600 /home/gitlab-runner/.ssh/id_rsa
  else
    echo "id_rsa secret not present. Set it to use remote Docker machine"
  fi
  # Docker config.json secret
  if [[ -f "/run/secrets/config.json" ]]; then
    echo "Symlinking /run/secrets/config.json to ~/.docker/config.json"
    mkdir -p /root/.docker
    ln -s /run/secrets/config.json /root/.docker/config.json
  else
    echo "Docker config.json secret not present. Set it to use registry credentials"
  fi

  # Run gitlab-runner
  exec /usr/bin/dumb-init -- gitlab-runner run \
       --user=gitlab-runner \
       --working-directory=/home/gitlab-runner
fi

exec /usr/bin/dumb-init -- "$@"