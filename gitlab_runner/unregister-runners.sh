#!/bin/bash
set -o nounset
set -o errexit

## Script to remove accidentally registered runners with inactive status
## E.g. Kubernetes register and fail spawning 100's of runners
# Required tools:
# curl
# jq
# tr

gitlabUrl=https://xxxxx  # todo: point to gitlab url to cleanup
gitlabApiToken=""  # todo: fill with admin api token

# Which runners are online?
onlineRunnerIds=$(curl --fail --header "PRIVATE-TOKEN: ${gitlabApiToken}" "${gitlabUrl}/api/v4/groups/222/runners?scope=online&per_page=10000000000000000000000000000000000" | jq '.[].id' | tr '\n' ' ')
echo $onlineRunnerIds

# For all runners, except those that are online
offlineRunnerIds=$(curl --fail --header "PRIVATE-TOKEN: ${gitlabApiToken}" "${gitlabUrl}/api/v4/groups/222/runners?per_page=100000000000000000000000000000000000000000000000000" | jq '.[].id' | tr '\n' ' ')
echo $offlineRunnerIds

# Remove runners
for id in ${offlineRunnerIds}; do
    echo "deleting [${id}]"
    curl --fail --request DELETE --header "PRIVATE-TOKEN: ${gitlabApiToken}" "${gitlabUrl}/api/v4/runners/${id}"
done
