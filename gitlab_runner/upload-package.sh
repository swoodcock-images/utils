#!/bin/bash
set -o nounset
set -o errexit

## Script to upload to Gitlab generic registry
## Useful for adding artifacts to releases
# Required tools:
# curl

gitlabUrl=  # todo: point to gitlab url to cleanup
gitlabApiToken=""  # todo: fill with admin api token
projectId= # todo: add project ID
filePath= # todo: add file path to upload
objectName= # todo: add name of object in registry
releaseVersion= # todo add version of release associated with


# Get file name
fileName="$(basename -- $filePath)"

echo "Uploading file $fileName v$releaseVersion to $gitlabUrl as $objectName"
curl --header "PRIVATE-TOKEN: $gitlabApiToken" \
     --upload-file $filePath \
     "$gitlabUrl/api/v4/projects/$projectId/packages/generic/$objectName/$releaseVersion/$fileName"
echo "URL: $gitlabUrl/api/v4/projects/$projectId/packages/generic/$objectName/$releaseVersion/$fileName"

