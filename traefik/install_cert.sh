#/bin/bash
set -eo pipefail

sudo cp ./certs/cert.crt /usr/local/share/ca-certificates/cert.crt

sudo update-ca-certificates

sudo rm /usr/local/share/ca-certificates/cert.crt
