#/bin/bash

sudo tee -a /etc/apt/sources.list <<EOF
http://ftp.debian.org/debian testing main
EOF

touch default-release
sudo tee /etc/apt/apt.conf.d/default-release <<EOF
APT::Default-Release "bullseye"
EOF

sudo apt update
sudo apt -t testing install -y podman

# Workaround for no systemctl or journalctl
sudo tee /etc/containers/containers.conf <<EOF
[engine]
cgroup_manager = "cgroupfs"
events_logger = "file"
EOF

# Allow privileged port usage
sudo tee -a /etc/sysctl.conf <<EOF
net.ipv4.ip_unprivileged_port_start=0
EOF
sudo sysctl -p

# Workaround required for Debian nftables, as Podman uses legacy iptables
sudo update-alternatives --set iptables /usr/sbin/iptables-legacy
sudo update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy
