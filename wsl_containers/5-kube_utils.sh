#/bin/bash

echo "Installing Kubectl"
cd $(mktemp -d)
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo mv kubectl /usr/local/bin/kubectl
sudo chmod +x /usr/local/bin/kubectl

echo "Installing Helm"
cd $(mktemp -d)
curl -LO https://get.helm.sh/helm-v3.10.2-linux-amd64.tar.gz
tar -xvzf helm-v3.10.2-linux-amd64.tar.gz
sudo mv linux-amd64/helm /usr/local/bin/helm

echo "Installing Kubie"
cd $(mktemp -d)
curl -LO https://github.com/sbstp/kubie/releases/download/v0.19.1/kubie-linux-amd64
sudo mv kubie-linux-amd64 /usr/local/bin/kubie
sudo chmod +x /usr/local/bin/kubie

echo "Adding aliases to .bashrc"
echo alias k='kubectl' >> ~/.bashrc
echo alias kcc="'kubie ctx'" >> ~/.bashrc
echo alias ns="'kubie ns'" >> ~/.bashrc
source ~/.bashrc
