#/bin/bash

echo "Installing Fish shell"
echo 'deb http://download.opensuse.org/repositories/shells:/fish:/release:/3/Debian_11/ /' | sudo tee /etc/apt/sources.list.d/shells:fish:release:3.list
curl -fsSL https://download.opensuse.org/repositories/shells:fish:release:3/Debian_11/Release.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/shells_fish_release_3.gpg > /dev/null
sudo apt update
sudo apt install fish

# Fish Config
sudo tee -a ~/.config/fish/config.fish <<EOF
# Starting Docker daemon automatically if not running...
set RUNNING (ps aux | grep dockerd | grep -v grep)
if [ -z "$RUNNING" ]
    sudo dockerd > /dev/null 2>&1 &
    disown
end
# Starting Tailscale daemon automatically if not running...
set RUNNING (ps aux | grep tailscaled | grep -v grep)
if [ -z "$RUNNING" ]
    sudo tailscaled > /dev/null 2>&1 &
    disown
end
# Starting SSH daemon automatically if not running...
set RUNNING (ps aux | grep sshd | grep -v grep)
if [ -z "$RUNNING" ]
    sudo service ssh start > /dev/null 2>&1 &
    disown
end
alias dc='docker compose'
alias k='kubectl'
alias kcc='kubie ctx'
alias ns='kubie ns'
alias commit='cz commit'
alias bump='cz bump --check-consistency --changelog'
function copy-secret
    kubectl get secret $argv[1] -o json \
    | jq 'del(.metadata["namespace","creationTimestamp","resourceVersion","selfLink","uid"])' \
    | kubectl apply -n $argv[2] -f -
end
EOF
